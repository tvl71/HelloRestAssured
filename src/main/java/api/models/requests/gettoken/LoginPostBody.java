package api.models.requests.gettoken;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class LoginPostBody{

	@JsonProperty("password")
	private String password;

	@JsonProperty("app_key")
	private String appKey;

	@JsonProperty("email")
	private String email;

	public LoginPostBody(String password, String appKey, String email) {
		this.password = password;
		this.appKey = appKey;
		this.email = email;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setAppKey(String appKey){
		this.appKey = appKey;
	}

	public String getAppKey(){
		return appKey;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	@Override
 	public String toString(){
		return 
			"LoginPostBody{" + 
			"password = '" + password + '\'' + 
			",app_key = '" + appKey + '\'' + 
			",email = '" + email + '\'' + 
			"}";
		}
}