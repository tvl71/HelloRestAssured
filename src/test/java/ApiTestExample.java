import api.models.requests.gettoken.LoginPostBody;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isEmptyString;

public class ApiTestExample {

    private LoginPostBody body;
    private String bodyString;

    @BeforeClass
    private void setUpBefore() {
        RestAssured.baseURI = "https://flydev.rocketroute.com/";
        body = new LoginPostBody("qwerty1", "2fabf2a3ffe33cb3e384b4028d0757f1", "ordertestpersonal@mailinator.com");
        bodyString = "{\n" +
                "    \"password\": \"qwerty1\",\n" +
                "    \"email\": \"ordertestpersonal@mailinator.com\",\n" +
                "    \"app_key\": \"2fabf2a3ffe33cb3e384b4028d0757f1\"\n" +
                "}";
    }

    @Test
    private void testCanRegisterValidUser() {

        RestAssured.given()
                .contentType(ContentType.JSON)
                .header(new Header("X-API-Version", "1.0"))
                .log().all()
                .body(bodyString)
                .when().post("api/login")
                .then().log().all()
                .assertThat()
                .statusCode(200)
                .body("id", not(isEmptyString()))
                .body("status.success", equalTo(true));
    }

    @Test
    private void testCanLoginValidUser() {

        Response response = setUp()
                .body(body)
                .when().post("/api/login")
                .then().extract().response();

        Assert.assertEquals(response.statusCode(), 200);
        Integer idValue = response.body().jsonPath().getJsonObject("data.id");

        Assert.assertEquals(idValue, (Integer)131621, "" );

        assertThat(response.asString(), matchesJsonSchemaInClasspath("loginResponse.json"));

    }


    private RequestSpecification setUp() {
        return RestAssured
                .given()
                .contentType(ContentType.JSON)
                .filters(new RequestLoggingFilter(),
                        new ResponseLoggingFilter())
                ;
    }


}
